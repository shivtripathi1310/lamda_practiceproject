package org.tmb.part19_predicate;

import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.function.*;
import java.util.stream.Collectors;

/**
 * Predicate Functional Interface
 * 1. boolean test(T)
 * 2. It takes 1 arg
 * 3. Return type is always boolean
 *
 * Other Function Functional Interface are :
 * 1. UnaryOperator
 * 2. BinaryOperator
 * 3. BiFunction
 * 4. DoubleFunction
 */
public class TestRunner01 {

    //Here, both are same, If both argument and return type are same , then it is better to use UnaryOperator
    Function<Integer, Integer> increment = num -> num + 1;
    UnaryOperator<Integer> incrementUnaryOperator = num -> num + 1;

    //Here, both are same, If both 2 arguments and return type are same , then it is better to use BinaryOperator
    BiFunction<Integer, Integer, Integer> incrementBi = (num1, num2) -> num1 + num2 + 1;
    BinaryOperator<Integer> incrementBinaryOperator =(num1, num2) -> num1 + num2 + 1;

    Function<Integer, Integer> multi5 = num -> num * 5;

    Predicate<Integer> isEven = i -> i%2==0;
    Predicate<Integer> isOdd = isEven.negate();
    Predicate<Integer> isDivisibleBy3 = i -> i%3==0;
    Predicate<Integer> isDivisibleBy6 = isEven.and(isDivisibleBy3);

    Predicate<String> isSWa = str -> str.startsWith("a");
    Predicate<String> isSWb = str -> str.startsWith("b");
    Predicate<String> isSWaORisSWb = isSWa.or(isSWb);


    @Test
    public void predicateTest(){
        System.out.println(isEven.test(2));
        System.out.println(isOdd.test(2));

        System.out.println(isDivisibleBy6.test(6));

        System.out.println(isSWaORisSWb.test("ahiv"));

        List<Integer> integerList = List.of(1,2,3,4,5,6);
        List<Integer> evenList01 = integerList.parallelStream().filter(isEven).collect(Collectors.toList());
        System.out.println(evenList01);
//        Using partitioningBy
        Map<Boolean, List<Integer>> evenList02 = integerList.parallelStream().collect(Collectors.partitioningBy(isEven));
        System.out.println(evenList02.get(true));      //even
        System.out.println(evenList02.get(false));     //odd
    }
}
