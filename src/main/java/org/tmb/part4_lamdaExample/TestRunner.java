package org.tmb.part4_lamdaExample;

public class TestRunner {

    public static void main(String[] args) {
        // Anonymous class - Implementation
        ISample iSample01 = new ISample() {
            @Override
            public void print() {
                System.out.println("Innner class -01");
            }
        };

        // Lambda - Implementation
        // 1. Lambda can only be used for Functional Interface.
        // 2. Interface with only 1 abstract method.
        // 3. Argument of Interface and return type should match for the lamda expression
        ISample iSample02 = () -> System.out.println("Lambda Implementation");

        iSample01.print();
        iSample02.print();
    }
}
