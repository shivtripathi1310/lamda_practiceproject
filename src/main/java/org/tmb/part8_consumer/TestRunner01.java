package org.tmb.part8_consumer;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class TestRunner01 {

    public static void main(String[] args) {
        // Example 01 - Cousumer Interface
        getStringOperation((str) -> System.out.println(str.length()), "WhatIsMyLength");
        getStringOperation((str) -> System.out.println(str.toLowerCase()), "WhatIsMyLengthUpper");

        // Example 02 - foreach
        List<Integer> list= Arrays.asList(3,43);
        Consumer<String> stringConsumer= str -> System.out.println(str); // ? --> String or superclass of String.
        Consumer<Integer> intConsumer= e -> {
            if(e%2==0)
                System.out.println("Even");
            else
                System.out.println("Odd");
        }; // ? --> Integer or superclass of Integer.
        list.forEach(intConsumer.andThen(e -> System.out.println(e)));

        // Example 03 - WebDriverManager
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");

        List<WebElement> elements = driver.findElements(By.xpath("//a"));
        elements.forEach( e -> {
            if(!e.getText().isBlank())
                System.out.println(e.getText());
        });
//        driver.quit();

    }

    private static void getStringOperation(Consumer<String> consumer, String str) {
        consumer.accept(str);
    }
}