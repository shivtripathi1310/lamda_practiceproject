package org.tmb.part8_consumer;

import java.util.HashMap;
import java.util.Map;

/**
 * This is an Example of Biconsumer - 2 arguments are consumed/taken here
 * It is used with Maps
 *
 * Other Consumer Interface Examples :
 * 1. DoubleConsumer - Here, argument sent is of Double type
 * 2. IntConsumer - Here, argument sent is of Int type
 * 3. LongConsumer - Here, argument sent is of Long type
 * 4. ObjIntConsumer - Here, argument sent is of Object type and Int type
 * 5. ObjDoubleConsumer - Here, argument sent is of Object type and Double type
 * 6. ObjLongConsumer - Here, argument sent is of Object type and Long type
 *
 */
public class TestRunner03 {

    public static void main(String[] args) {
        Map<Integer, String> map= new HashMap<>();
        map.put(1,"Shiv");
        map.put(2,"Prakash");
        map.put(3,"Tripathi");

        String str = "aabdcsae";

        Map<Character, Integer> mapDuplicate= new HashMap<>();

        char[] charArray = str.toCharArray();
        for(char ch : charArray){
            if(mapDuplicate.containsKey(ch)){
                mapDuplicate.put(ch,mapDuplicate.get(ch)+1);
            }else
                mapDuplicate.put(ch,1);
        }

        // Biconsumer Interface, 2 arguments are taken
        mapDuplicate.forEach((k,y)-> System.out.println(k+":"+y));

        // Consumer Interface , 1 arguments is taken
        map.entrySet().forEach(e-> System.out.println(e.getKey() +":"+e.getValue()));
    }
}
