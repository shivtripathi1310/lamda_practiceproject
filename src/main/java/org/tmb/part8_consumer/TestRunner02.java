package org.tmb.part8_consumer;

import com.google.common.util.concurrent.Uninterruptibles;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TestRunner02 {

    public static void main(String[] args) {

        // Consumer Example

        WebDriverManager.chromedriver().setup();
        WebDriver driver= new ChromeDriver();
        driver.get("https://demoqa.com/select-menu");
        driver.manage().window().maximize();

        WebElement element = driver.findElement(By.xpath("//select[@id='oldSelectMenu']"));
        selectFromDropdown(select -> select.selectByValue("2"),element);
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);

        selectFromDropdown(select -> select.selectByVisibleText("Red"),element);
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);

        selectFromDropdown(select -> select.selectByIndex(4),element);
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);


        WebElement multiSelect = driver.findElement(By.xpath("//select[@id='cars']"));
        Select select = new Select(multiSelect);
        select.getOptions().stream()
                        .skip(2).forEach(e -> e.click());
        driver.quit();
    }

    private static void selectFromDropdown(Consumer<Select> consumer,WebElement element){
        consumer.accept(new Select(element));
    }
}
