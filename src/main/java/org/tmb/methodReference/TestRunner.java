package org.tmb.methodReference;

public class TestRunner {
    public static void main(String[] args) {

        //01
        TaskInterface taskInterface = () -> System.out.println("Display 01 called");
        taskInterface.display();

        //02
        TaskInterface taskInterface1 = Keyword::display;
        taskInterface1.display();

        //03
        TaskInterface taskInterface2 = Keyword::display02;
        taskInterface2.display();

        //04
        TaskInterface taskInterface3 = new Keyword()::display03;
        taskInterface3.display();
    }
}
