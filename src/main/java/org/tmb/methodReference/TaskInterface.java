package org.tmb.methodReference;

@FunctionalInterface
public interface TaskInterface {

    public void display();
}
