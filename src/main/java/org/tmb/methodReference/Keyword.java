package org.tmb.methodReference;

public class Keyword {

    public static void display(){
        System.out.println("Static Method Reference called : display01");
    }

    public static void display02(){
        System.out.println("Static Method Reference called : display02");
    }

    public void display03(){
        System.out.println("Default Method Reference called : display03");
    }
}
