package org.tmb.part3_anonymousInnerClass;

/**
 * Anonymous Inner class is used when Implementation method is not getting reused more than 1 time.
 * Thus, Inner class can be created with different implementations.
 */
public class TestRunner {

    public static void main(String[] args) {
        ISample iSample01= new ISample() {
            @Override
            public void print() {
                System.out.println("Innner class -01");
            }
        };

        ISample iSample02= new ISample() {
            @Override
            public void print() {
                System.out.println("Innner class -02");
            }
        };

        iSample01.print();
        iSample02.print();
    }
}
