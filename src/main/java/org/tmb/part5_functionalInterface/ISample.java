package org.tmb.part5_functionalInterface;

/**
 * An Interface is called Functional Interface if it follows properties :
 * 1. Only 1 abstract method.
 * 2. Can have =>0 static methods.
 * 3. Can have =>0 defaults methods.
 *
 * All abstract, default, and static methods in an interface are implicitly public, so you can omit the public modifier.
 * i.e public void print(); OR void print(); -> Both are same.
 */
@FunctionalInterface
public interface ISample {

    void print();

    default void sampleDefault(){
        System.out.println("Default method called");
    }

    static void display(){
        System.out.println("Static method called");
    }
}
