package org.tmb.part5_functionalInterface;

public class TestRunner {

    public static void main(String[] args) {

        // Lambda - Implementation
        // 1. Lambda can only be used for Functional Interface.
        // 2. Interface with only 1 abstract method.
        // 3. Argument of Interface and return type should match for the lamda expression
        ISample iSample02 = () -> System.out.println("Lambda Implementation");

        iSample02.sampleDefault();      // Default method called
        ISample.display();              // Static method called
        iSample02.print();              // Abstract method called
    }
}
