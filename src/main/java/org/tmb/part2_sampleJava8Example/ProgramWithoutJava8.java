package org.tmb.part2_sampleJava8Example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.*;

/**
 * 1. Fetch all the links from amazon
 * 2. Get all the text and it should not be Empty
 * 3. Remove the Duplicates
 * 4. Sort the Text
 * 5. Fetch text that start with C and D
 */
public final class ProgramWithoutJava8 {

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.get("https://amazon.com");
        List<WebElement> elements=driver.findElements(By.xpath("//a"));

        List<String> elementText = new ArrayList<>();

        for(WebElement ele : elements){
            String value = ele.getText();
            if(!value.isBlank()) {
                elementText.add(ele.getText());
            }
        }

        List<String> duplicateTextRemovedText = new ArrayList<>(new HashSet<String>(elementText));
        Collections.sort(duplicateTextRemovedText);

        for(String str : duplicateTextRemovedText){
            if(str.startsWith("C") || str.startsWith("D")){
                System.out.println(str + "\n");
            }
        }

        driver.quit();
    }
}
