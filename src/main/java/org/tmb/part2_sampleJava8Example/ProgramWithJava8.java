package org.tmb.part2_sampleJava8Example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * 1. Fetch all the links from amazon
 * 2. Get all the text and it should not be Empty
 * 3. Remove the Duplicates
 * 4. Sort the Text
 * 5. Fetch text that start with C and D
 */
public final class ProgramWithJava8 {

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.get("https://amazon.com");
        driver.findElements(By.xpath("//a"))
                .stream()
                .map(e -> e.getText())
                .filter(e -> !e.isBlank())
                .distinct()
                .sorted()
                .filter(e -> e.startsWith("C") || e.startsWith("D"))
                .forEach(System.out::println);

        driver.quit();
    }
}
