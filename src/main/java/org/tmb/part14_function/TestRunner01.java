package org.tmb.part14_function;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Function Interface
 * 1. Abstract method : R apply(T)
 * 2. It takes 1 argument
 * 3. Returns 1 argument
 * <p>
 * Other Function Functional Interface are :
 * 1. UnaryOperator
 * 2. BinaryOperator
 * 3. BiFunction
 * 4. DoubleFunction
 */
public class TestRunner01 {

    //Here, both are same, If both argument and return type are same , then it is better to use UnaryOperator
    private static Function<Integer, Integer> increment = num -> num + 1;
    private static UnaryOperator<Integer> incrementUnaryOperator = num -> num + 1;

    //Here, both are same, If both 2 arguments and return type are same , then it is better to use BinaryOperator
    private static BiFunction<Integer, Integer, Integer> incrementBi = (num1, num2) -> num1 + num2 + 1;
    private static BinaryOperator<Integer> incrementBinaryOperator =(num1, num2) -> num1 + num2 + 1;

    private static Function<Integer, Integer> multi5 = num -> num * 5;

    public static void main(String[] args) {

    }
}
