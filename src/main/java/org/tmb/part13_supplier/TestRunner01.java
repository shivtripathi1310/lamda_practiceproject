package org.tmb.part13_supplier;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Supplier Functional Interface3
 * 1. Abstract method : T get()
 * 2. No Arguments
 * 3. Return type is present
 * 4. Default methods: NA
 * 5. BiType : NA
 * 6. What is Lazy evaluation? How to perform that using Lambdas ?
 * 7. Use case 01 : Assertion libraries
 * 8. Use case 02 : Explicit wait messages
 * 9. Use case 03 : Factory Method Design Pattern
 */
public class TestRunner01 {

    public static void main(String[] args) {
        // Lazy Evaluation
        // compute(isElememnt01() && isElement02()) -> here if both isElememnt01() and isElememnt02() are lambda functions. Then lazy evaluation will happen
        // i.e if isElememnt01() -> False, then isElememnt02() will not be computed
        // while isElememnt01() is not lambda , then isElememnt01() and isElememnt02() both will be computed.

        // Use case 01 - Assertions with junit we can use Supplier.
        // Use case 02 - Explicit wait message
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.withMessage(() -> "Wait").until(dr -> dr.findElement(By.xpath("")).isDisplayed());

    }
}
