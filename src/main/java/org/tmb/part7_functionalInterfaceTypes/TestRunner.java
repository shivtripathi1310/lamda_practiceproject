package org.tmb.part7_functionalInterfaceTypes;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class TestRunner {

    public static void main(String[] args) {
        // 1. Counsumer FI
        // It takes an Argument and do the processing for that argument
        // No Return type
        // Abstract function : void accept(T t)
        Consumer<String> consumer = (e) -> System.out.println("consumer " + e);
        consumer.accept("Shivpt");

        // 2. Predicate FI
        // It takes an argument , perform the process and returns True or False
        // Return Type: True or False
        // Abstract Function : boolean test(T t);
        Predicate<Integer> predicate = (e) -> {
            if(e==1)
                return true;
            else
                return false;
        };
        System.out.println(predicate.test(2));

        // 3. Function FI
        // It takes an argument , perform te process and return the value.
        // Return type : Value
        // Abstract function : R apply(T t);
        Function<String, Integer> length = (str) -> str.length();
        System.out.println(length.apply("shiv"));

        // 4. Supplier FI
        // It does not takes any argument, perform the process and return the value.
        // Return type : Value
        // Abstract Function : R get();
        Supplier<Integer> supplier= ()-> {
            String str = "Hello";
            return  str.length();
        };
        System.out.println(supplier.get());
    }
}
