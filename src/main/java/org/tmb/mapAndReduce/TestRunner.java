package org.tmb.mapAndReduce;

import java.util.OptionalInt;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class TestRunner {
    public static void main(String[] args) {
        Supplier<Stream<Integer>> supplier = () -> Stream.of(1,3,4,5,6,7);

        int max = supplier.get().reduce(0,(Integer::max));
        OptionalInt max01 = supplier.get().mapToInt(i->i).max();;
        System.out.println(max);

        int min = supplier.get().reduce(99,(a,b)->a<b?a:b);
        int min01 = supplier.get().reduce(99,Integer::min);
        OptionalInt min02 = supplier.get().mapToInt(i->i).min();;
        System.out.println(min);
        System.out.println(min01);

//        double avg = supplier.get().mapToDouble(i->i).reduce(0,(a, b) -> (a+b)/2);   It is incorrect way to get average.
        double avg01 = supplier.get().mapToDouble(i->i).average().getAsDouble();
//        System.out.println(avg);
        System.out.println(avg01);

        int sum = supplier.get().reduce(0,Integer::sum);
        System.out.println(sum);
        int sum01 = supplier.get().mapToInt(i->i).sum();
        System.out.println(sum01);
    }
}
